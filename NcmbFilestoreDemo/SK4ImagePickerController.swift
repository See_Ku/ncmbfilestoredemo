//
//  SK4ImagePickerController.swift
//  NcmbFilestoreDemo
//
//  Created by See.Ku on 2016/03/07.
//  Copyright (c) 2016 AxeRoad. All rights reserved.
//

import UIKit

/// UIImagePickerControllerを簡単に使うためのクラス
public class SK4ImagePickerController: UIImagePickerController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

//	let check = SK4LeakCheck(name: "SK4ImagePickerController")

	// /////////////////////////////////////////////////////////////
	// MARK: - プロパティ

	/// 画像を取得したときの処理　※dismissViewController確定
	public var execFinishPicking: ((image: UIImage?, info: [String : AnyObject])->Void)?

	/// キャンセルされたときの処理　※dismissViewController確定
	public var execCancel: (()->Void)?

	/// 取得できたイメージ
	var pickImage: UIImage?

	// /////////////////////////////////////////////////////////////
	// MARK: - メソッド

	/// SourceTypeを指定して初期化
	public convenience init(sourceType: UIImagePickerControllerSourceType) {
		self.init()

		self.sourceType = sourceType
		self.delegate = self

		if sourceType != .Camera {
			modalPresentationStyle = .FormSheet
		}
	}

	/// 画像を取得できた
	public func onFinishPicking(image: UIImage?, info: [String : AnyObject]) {
		dismissViewControllerAnimated(true, completion: nil)
	}

	/// キャンセルされた
	public func onCancel() {
		dismissViewControllerAnimated(true, completion: nil)
	}

	// /////////////////////////////////////////////////////////////
	// MARK: - UIImagePickerControllerDelegate

	/// 画像を取得できた
	public func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
		pickImage = info[UIImagePickerControllerOriginalImage] as? UIImage
		if let exec = execFinishPicking {
			exec(image: pickImage, info: info)
			dismissViewControllerAnimated(true, completion: nil)
		} else {
			onFinishPicking(pickImage, info: info)
		}
	}

	/// キャンセルされた
	public func imagePickerControllerDidCancel(picker: UIImagePickerController) {
		if let exec = execCancel {
			exec()
			dismissViewControllerAnimated(true, completion: nil)
		} else {
			onCancel()
		}
	}

}

// eof
