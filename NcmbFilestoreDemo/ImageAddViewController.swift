//
//  ImageAddViewController.swift
//  NcmbFilestoreDemo
//
//  Created by See.Ku on 2016/03/09.
//  Copyright (c) 2016 AxeRoad. All rights reserved.
//

import UIKit
import NCMB

class ImageAddViewController: UIViewController {

	@IBOutlet weak var imageView: UIImageView!

	@IBAction func onSelect(sender: AnyObject) {

		// 画像ファイルを選択＆編集
		let vc = SK4ImagePickerController(sourceType: .SavedPhotosAlbum)
		vc.allowsEditing = true
		vc.execFinishPicking = { image, info in
			self.imageView.image = image
		}

		presentViewController(vc, animated: true, completion: nil)
	}

	@IBAction func onUpload(sender: AnyObject) {

		// 画像ファイルをJPEG形式でデータ化
		guard let image = imageView.image else { return }
		guard let data = UIImageJPEGRepresentation(image, 0.95) else { return }

		// ※本来ならここでサイズのチェックが必要

		// NCMBFileを生成
		let fn = NSUUID().UUIDString + ".jpg"
		let file = NCMBFile.fileWithName(fn, data: data)

		// アップロード
		file.saveInBackgroundWithBlock() { error in
			if let error = error {
				print("File save error : ", error)
			} else {
				print("File save OK: \(fn)")
			}
		}
	}

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}
