//
//  ViewController.swift
//  NcmbFilestoreDemo
//
//  Created by See.Ku on 2016/03/09.
//  Copyright (c) 2016 AxeRoad. All rights reserved.
//

import UIKit
import NCMB

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

	// /////////////////////////////////////////////////////////////
	// MARK: - Nifty Cloud Mobile Backend

	var fileArray: [NCMBFile]?
	var imageDic = [String: UIImage]()

	/// データを読み込み　※情報取得までは同期処理
	func readDataSync() {
		let query = NCMBFile.query()
		query.limit = 20

		do {
			fileArray = try query.findObjects() as? [NCMBFile]
			fetchData()

			print("read ok: \(fileArray?.count)")
		} catch {
			fileArray = nil

			print("read error: \(error)")
		}
	}

	/// データの取得処理を実行
	func fetchData() {
		guard let ar = fileArray else { return }

		for file in ar {

			// 取得済みのデータはスキップ
			if imageDic[file.name] != nil {
				print("imageDic: \(file.name)")
				continue
			}

			// データを取得
			file.getDataInBackgroundWithBlock() { data, error in
				if let error = error {
					print("get error: \(error)")

				} else {
					let image = UIImage(data: data)
					self.imageDic[file.name] = image
					self.collectionView.reloadData()

					print("get file.name: \(file.name)")
				}
			}
		}
	}


	// /////////////////////////////////////////////////////////////
	// MARK: - Interface

	@IBOutlet weak var collectionView: UICollectionView!


	////////////////////////////////////////////////////////////////
	// MARK: - UICollectionViewDataSource

	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		if let ar = fileArray {
			return ar.count
		} else {
			return 0
		}
	}

	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath)
		let iv = cell.viewWithTag(1) as! UIImageView
		if let ar = fileArray {
			let file = ar[indexPath.row]
			iv.image = imageDic[file.name]
		}
		return cell
	}

	////////////////////////////////////////////////////////////////
	// MARK: - UICollectionViewDelegateFlowLayout

	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
		let si = collectionView.bounds.size
		let min = (si.width < si.height) ? si.width : si.height
		let len = (min - 8 * 3) / 4
		return CGSize(width: len, height: len)
	}


	// /////////////////////////////////////////////////////////////
	// MARK: - other

	override func viewDidLoad() {
		super.viewDidLoad()

		collectionView.dataSource = self
		collectionView.delegate = self
	}

	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)

		readDataSync()
		collectionView.reloadData()
	}

	override func viewDidLayoutSubviews() {
		collectionView.collectionViewLayout.invalidateLayout()
		collectionView.reloadData()

		super.viewDidLayoutSubviews()
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

}

